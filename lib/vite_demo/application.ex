defmodule ViteDemo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      ViteDemo.Repo,
      # Start the Telemetry supervisor
      ViteDemoWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: ViteDemo.PubSub},
      # Start the Endpoint (http/https)
      ViteDemoWeb.Endpoint
      # Start a worker by calling: ViteDemo.Worker.start_link(arg)
      # {ViteDemo.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ViteDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    ViteDemoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
