defmodule ViteDemo.Repo do
  use Ecto.Repo,
    otp_app: :vite_demo,
    adapter: Ecto.Adapters.Postgres
end
