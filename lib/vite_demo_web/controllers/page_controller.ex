defmodule ViteDemoWeb.PageController do
  use ViteDemoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
